<?php

namespace App\Helpers\PagSeguro;

use PagSeguro\Configuration\Configure;

class Setters extends Configure{
    public static function setAllDependencies(){
        self::setEnvironment(config('services.pagseguro.Ambiente'));
        self::setAccountCredentials(
            config('services.pagseguro.Email'),
            config('services.pagseguro.Token')
        );
        self::setCharset(config('services.pagseguro.Charset'));
        self::setLog(true, realpath('../back-end/storage/logs/PagSeguro.log'));
    }
}