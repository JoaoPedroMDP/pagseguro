<?php
namespace App\Helpers\PagSeguro;

use PagSeguro\Library;
use Illuminate\Http\Exceptions\HttpResponseException;

class Initializers{
    public static function initializePagSeguro(){
        try{
            Library::initialize();
            Library::cmsVersion()->setName('PagSeguro')->setRelease('1.0.0');
            Library::moduleVersion()->setName('PagSeguro')->setRelease('1.0.0');
        }catch (Exception $e){
            throw new HttpResponseException(
                response()->json([
                    'Mensagem' => 'Erro ao inicializar o PagSeguro',
                    'Dados' => $e->getMessage()
                ],500)
            );
        }
    }
}