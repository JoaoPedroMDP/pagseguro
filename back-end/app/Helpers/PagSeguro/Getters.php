<?php
namespace App\Helpers\PagSeguro;

use PagSeguro\Configuration\Configure;

class Getters{
    public static function credentials(){
        return Configure::getAccountCredentials();
    }
}