<?php
namespace App\Helpers;

class Xml{
    public static function errorMessage($xmlString){
        return simplexml_load_string($xmlString)->error->message;
    }
}