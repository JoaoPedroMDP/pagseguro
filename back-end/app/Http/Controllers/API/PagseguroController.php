<?php

namespace App\Http\Controllers\API;

use App\Helpers\Xml;
use App\Helpers\PagSeguro\{ Initializers, Getters, Setters};
use App\Http\Controllers\Controller;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PagSeguro\Services\Session;
use PagSeguro\Configuration\Configure;
use PagSeguro\Domains\Requests\Payment;
use PagSeguro\Services\Transactions\{Cancel, Search, Refund};
use Search\{Reference, Code};
use Validator, Exception;

Initializers::initializePagSeguro();
Setters::setAllDependencies();
try{
    $sessionCode = Session::create(
        Configure::getAccountCredentials()
    );
}catch(Exception $e){
    return response()->json(['error', $e->getMessage()]);
}

class PagseguroController extends Controller
{
    const status = [
        'Aguardando pagamento', 'Em análise', 'Paga', 'Disponível',
        'Em disputa', 'Devolvida', 'Cancelada'
    ];

    /** Gera o código da compra, pro front poder redirecionar o malucão */
    public function checkout(Request $request){
        $validator = Validator::make($request->all(), [
            'itemId' => 'required|integer',
            'itemDescription' => 'required|string',
            'itemAmount' => 'required|integer',
            'itemPrice' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(['Error', 'Campos preenchidos incorretamente!', $validator->errors()]);
        }

        $payment = new Payment();
        $payment->addItems()->withParameters(
            $request->itemId,
            $request->itemDescription,
            $request->itemAmount,
            $request->itemPrice
        );

        $payment->setCurrency('BRL');
        $payment->setReference('iaew');
        
        // Outras configs

        try{
            $onlyCheckoutCode = true;
            $response = $payment->register(
                Configure::getAccountCredentials(),
                $onlyCheckoutCode
            );
            return response()->json([
                'Message' => 'Código gerado com sucesso!',
                'Code' => $response->getCode()
            ], 200);
        }catch(Exception $e){
            return response()->json(Xml::errorMessage($e->getMessage()), 400);
        }
    }

    /** Lista as transações de um usuário */
    public function lastTransactions($id){
        // Apenas se tiver código para cada usuário
        // $user = User::find($id);
        $initialDate = Carbon::now()->subMonths(1)->format('Y-m-d\TG:i');
        $options = [
            'initial_date' => $initialDate,
            //'final_date' => $seilá,
            'page' => 1,
            'max_per_page' => 15
        ];

        try{
            $response = Reference::search(Configure::getAccountCredentials(), 'iaew', $options);
            $transactions = $response->getTransactions();

            $array = collect();
            if ($transactions){
                foreach($transactions as $transaction){
                    $date = new Carbon($transaction->getDate());
                    $data = [
                        'date' => $date->format('d/m/Y H:i:s'),
                        'transactionCode' => $transaction->getCode(),
                        'value' => $transaction->getGrossAmount(),
                        'status' => $this::status[$transaction->getStatus() - 1]
                    ];
                    $array->push($data);
                }
            }
            return response()->json([$array->all(), 'Transações listadas com sucesso']);
        }catch(Exception $e){
            return response()->json(['Ocorreu um erro listando as transações', $e->getMessage()]);
        }
    }

    /** Retorna dados sobre uma transação em específico */
    public function aSpecificTransaction(Request $request){
        $validator = Validator::make($request->all(), [
            'transaction_id' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'Message' => 'Campos preenchidos incorretamente!',
                'Dados' => $validator->errors()
            ]);
        }

        try{
            $response = Code::search(Configure::getAccountCredentials(), $request->transaction_id);
            $date = new Carbon($response->getDate());
            $date = $date->toDateTimeString();
            $grossAmount = $response->getGrossAmount();
            $items = $response->getItems();
            $array = array();
            foreach($items as $item){
                $new = [
                    'Amount' => $item->getAmount(),
                    'Quantity' => $item->getQuantity(),
                    'Description' => $item->getDescription(),
                ];
                array_push($array, $new);
            }
            $responseData = array($date, $grossAmount, $array);
            return response()->json($responseData);
        }catch(Exception $e){
            return response()->json([
                'Message' => 'Campos preenchidos incorretamente!',
                'Dados' => $e->getMessage()
            ]);
        }
    }

    /** Cancela uma transação (precisa ser antes de pagar) */
    public function cancel($transaction_code){
        try{
            $response = Cancel::create(Getters::credentials(), $transaction_code);
            return response()->json($response);
        }catch(Exception $e){
            return response()->json(Xml::errorMessage($e->getMessage()));
        }
    }

    public function refund(Request $request, $transaction_code){
        $amount = $request->valor;

        try{
            $response = Refund::create(Getters::credentials(), $transaction_code);
            Cache::put('responsePag', $response);
            return response()->json('Estorno feito com sucesso!');
        }catch(Exception $e){
            return response()->json(Xml::errorMessage($e->getMessage()));
        }
    }
}
