<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
    Rotas de login.
*/
Route::post('login', 'API\AuthController@login');
Route::post('signup', 'API\AuthController@signup');

Route::post('checkout', 'API\PagseguroController@checkout');
Route::get('lastTransactions/{id}', 'API\PagseguroController@lastTransactions');
Route::get('getTransaction', 'API\PagseguroController@aSpecificTransaction');
Route::delete('cancel/{transaction_code}', 'API\PagseguroController@cancel');
Route::put('refund/{transaction_code}', 'API\PagseguroController@refund');

/* 
    Rotas de autenticação.
*/
Route::middleware(['auth:api'])->group(function () {

    Route::get('logout', 'API\AuthController@logout');
    Route::get('user', 'API\AuthController@user');

});

